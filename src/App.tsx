import React from 'react';
import { UserPage } from './pages/UserPage';
import { BrowserRouter, NavLink, Route } from 'react-router-dom';
import { TodosPage } from './pages/TodosPage';
import { UserItemPage } from './pages/UserItemPage';
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div>
          <NavLink to={'/users'}>USERS</NavLink>
          <NavLink to={'/todos'}>TODOS</NavLink>
        </div>
        <Route path={'/users'} exact>
          <UserPage />
        </Route>
        <Route path={'/users/:id'} exact>
          <UserItemPage />
        </Route>
        <Route path={'/todos'} exact>
          <TodosPage />
        </Route>
      </div>
    </BrowserRouter>
  );
}

export default App;
