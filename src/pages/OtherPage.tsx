import React, { FC } from 'react';
import { Card, CardVariant } from '../Components/Card/Card';

export interface IOtherPageProps {}
export const OtherPage: FC<IOtherPageProps> = () => {
  return (
    <>
      <Card
        variant={CardVariant.outlined}
        width={'200px'}
        height={'100px'}
        onClick={(number) => console.log('I`am outlined card', number)}
      >
        Это новая карта outlined
        <button>Click on me</button>
      </Card>
      <Card variant={CardVariant.primary} width={'200px'} height={'100px'}>
        Это новая карта primary
      </Card>
    </>
  );
};
