import React, { FC, useEffect, useState } from 'react';
import { IUser } from '../Components/types/types';
import axios from 'axios';
import { UserList } from '../Components/UserList/UserList';
import { useHistory } from 'react-router-dom';
export interface IUserPageProps {
  className?: string;
}
export const UserPage: FC<IUserPageProps> = (props) => {
  const [users, setUsers] = useState<IUser[]>([]);
  const history = useHistory();

  useEffect(() => {
    fetchUsers();
  }, []);

  async function fetchUsers() {
    try {
      const response = await axios.get<IUser[]>('https://jsonplaceholder.typicode.com/users');
      setUsers(response.data);
    } catch (e) {
      alert(e);
    }
  }
  return <UserList users={users} />;
};
