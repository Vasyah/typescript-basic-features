import React, { FC, useEffect, useState } from 'react';
import { ITodo } from '../Components/types/types';
import axios from 'axios';
import { List } from '../Components/List/List';
import { TodoItem } from '../Components/Todo/TodoItem';

export interface ITodosPageProps {
  className?: string;
}
export const TodosPage: FC<ITodosPageProps> = (props) => {
  const [todos, setTodos] = useState<ITodo[]>([]);

  useEffect(() => {
    fetchTodos();
  }, []);

  async function fetchTodos() {
    try {
      const response = await axios.get<ITodo[]>(
        'https://jsonplaceholder.typicode.com/todos?_limit=10',
      );
      setTodos(response.data);
    } catch (e) {
      alert(e);
    }
  }

  return (
    <List<ITodo>
      items={todos}
      renderItem={(todo: ITodo) => <TodoItem todo={todo} key={todo.id} />}
    />
  );
};
