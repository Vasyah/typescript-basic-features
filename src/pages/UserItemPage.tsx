import React, { FC, useEffect, useState } from 'react';
import { IUser } from '../Components/types/types';
import axios from 'axios';
import { useHistory, useParams } from 'react-router-dom';
import { UserItem } from '../Components/UserList/UserItem';

export interface IUserItemsPageParams {
  id: string;
}

export const UserItemPage: FC = (props) => {
  const [user, setUser] = useState<IUser | null>(null);
  const params = useParams<IUserItemsPageParams>();
  const history = useHistory();
  useEffect(() => {
    fetchUser();
  }, []);

  async function fetchUser() {
    try {
      const response = await axios.get<IUser>(
        'https://jsonplaceholder.typicode.com/users/' + params.id,
      );
      setUser(response.data);
    } catch (e) {
      alert(e);
    }
  }

  if (!user) return <></>;
  return (
    <div>
      <button onClick={() => history.push('/users')}>back</button>
      <UserItem user={user} />
    </div>
  );
};
