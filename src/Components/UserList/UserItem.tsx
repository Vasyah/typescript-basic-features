import React, { FC } from 'react';
import { IUser } from '../types/types';

export interface IUserItemProps {
  user: IUser;
  onClick?: (user: IUser) => void;
}
export const UserItem: FC<IUserItemProps> = ({ user, onClick }) => {
  return (
    <div style={{ padding: '1rem' }} onClick={() => (onClick ? onClick(user) : null)}>
      <p>
        {user.id}: {user.name}
      </p>
      <p>Город: {user.address.city}</p>
      <p>Улица: {user.address.street}</p>
    </div>
  );
};
