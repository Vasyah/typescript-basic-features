import React, { FC } from 'react';
import { IUser } from '../types/types';
import { UserItem } from './UserItem';
import { useHistory } from 'react-router-dom';

export interface IUserListProps {
  users: IUser[];
}
export const UserList: FC<IUserListProps> = ({ users }) => {
  const history = useHistory();
  return (
    <div style={{ cursor: 'pointer' }}>
      {users.map((user) => (
        <UserItem
          key={user.id}
          user={user}
          onClick={(user: IUser) => history.push('/users/' + user.id)}
        />
      ))}
    </div>
  );
};
