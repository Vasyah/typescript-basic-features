import React, { FC } from 'react';
import { ITodo } from '../types/types';

export interface ITodoItemProps {
  todo: ITodo;
}
export const TodoItem: FC<ITodoItemProps> = ({ todo }) => {
  return (
    <div style={{ padding: '1rem' }}>
      <input id={String(todo.id)} type="checkbox" checked={todo.completed} />
      <label htmlFor={String(todo.id)}>
        {todo.id}: {todo.title}
      </label>
    </div>
  );
};
