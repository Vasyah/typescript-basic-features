import React from 'react';

export interface IListProps<T> {
  items: T[];
  renderItem: (item: T) => React.ReactNode;
}

export const List = <T extends {}>(props: IListProps<T>) => {
  return <div>{props.items.map((item) => props.renderItem(item))}</div>;
};
