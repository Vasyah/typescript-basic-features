import React, { FC, useState } from 'react';

export enum CardVariant {
  outlined = 'outlined',
  primary = 'primary',
}

export interface ICardProps {
  width?: string;
  height?: string;
  variant?: CardVariant;
  onClick?: (number: number) => void;
}
export const Card: FC<ICardProps> = ({ width, height, variant, onClick, children }) => {
  const [state, setState] = useState(0);
  return (
    <div
      style={{
        width,
        height,
        border: variant === CardVariant.outlined ? '2px solid gray' : 'none',
        backgroundColor: variant === CardVariant.outlined ? 'none' : 'lightgray',
      }}
      onClick={() => (onClick ? onClick(state) : null)}
    >
      {children}
    </div>
  );
};
