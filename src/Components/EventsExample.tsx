import React, { FC, useRef, useState } from 'react';

export interface IEventsExampleProps {
  className?: string;
}

export const EventsExample: FC<IEventsExampleProps> = (props) => {
  const [value, setValue] = useState('');
  const [isDrag, setIsDrag] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => setValue(e.target.value);
  const clickHandler = (e: React.MouseEvent<HTMLButtonElement>) => console.log(e);
  const dragHandler = (e: React.DragEvent<HTMLDivElement>) => {
    console.log('DRAG');
  };

  const dragOverhandler = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setIsDrag(true);
  };

  const dragLeaveHandler = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setIsDrag(false);
  };

  const dropHandler = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setIsDrag(false);
    console.log('DROP');
  };

  const onUncontrollClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    console.log(inputRef?.current?.value);
  };
  return (
    <div>
      <input type="text" ref={inputRef} placeholder={'uncontrolled'} />
      <input type="text" value={value} onChange={changeHandler} placeholder={'controlled'} />
      <button onClick={clickHandler}>Check controlled</button>
      <button onClick={onUncontrollClick}>Check uncontrolled</button>
      <div
        onDrag={dragHandler}
        draggable
        style={{ padding: '1rem', width: 200, height: 200, backgroundColor: 'red' }}
      />
      <div
        onDrop={dropHandler}
        onDragLeave={dragLeaveHandler}
        onDragOver={dragOverhandler}
        style={{
          padding: '1rem',
          width: 200,
          height: 200,
          backgroundColor: isDrag ? 'blue' : 'red',
        }}
      />
    </div>
  );
};
